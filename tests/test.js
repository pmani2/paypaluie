/**
 * Created by Pradeep on 12/23/2015.
 */

var assert = require('assert');
var should = require('should');
describe('Array', function() {
    describe('#indexOf()', function () {
        it('should return -1 when the value is not present', function () {
            assert.equal(1, [1,2,3].indexOf(2));
            assert.equal(-1, [1,2,3].indexOf(0));
        });

        it('should library is installed', function () {
            true.should.eql(true);
        });
    });
});
