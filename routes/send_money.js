/**
 * Created by Pradeep on 12/23/2015.
 */
var express = require('express');
var router = express.Router();

var save_transaction = function(res, email, sign, amount, message, trans_for){
    var database = require('./database');
    database.save_transaction(email, sign, amount, message, trans_for);
    res.render('trans_success', {title: 'Success', email: email, amount: amount, sign: sign});
};

router.get('/', function (req, res, next) {
    res.render('send_money', {title: 'Send Money'});
});

router.post('/', function (req, res) {
    var email = req.body.email,
        email_flag = false;
    var amount = req.body.amount,
        amount_flag = false

    var currency = req.body.currency;
    var message = req.body.message;
    var trans_for = req.body.trans_for;

    var error = false;

    if (! /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(email)) {
        error = true;
        email_flag = true;
    }

    if (! amount > 0) {
        error = true;
        amount_flag = true;
    }

    var sign = "&#36;";

    if (currency == "EUR") {
        sign = "&#8364;";
    } else if (currency == "JPY") {
        sign = "&#165;";
    }

    if (error) {
        res.render('send_money', {
            title: 'Send Money', email: email, email_flag: email_flag, amount: amount,
            amount_flag: amount_flag, currency: currency, sign: sign, message: message, trans_for: trans_for
        });
    } else{
        save_transaction(res, email, sign, amount, message, trans_for);
    }

});

module.exports = router;
