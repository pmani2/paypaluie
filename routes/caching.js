/**
 * Created by Pradeep on 12/28/2015.
 */

var fs = require("fs");
var fileName = "./mock_transactions.json";
var cache = require('memory-cache');

module.exports = {
    // Deletes the cached data if there is any change
    watch_trans_file: function () {
        fs.watch(fileName, {
            persistent: true
        }, function (event, filename) {
            cache.del('trans_data');
        });
    },
    read_trans_data: function (res) {
        fs.exists(fileName, function (exists) {
            if (exists) {
                fs.readFile(fileName, 'utf8', function (err, data) {
                    if (err) {
                        return console.log(err);
                    }
                    cache.put('trans_data', data);
                    res.render("transactions_history", {title: "Transactions History", data: data});
                });
            }
            else {
                console.log("file not exists");
                res.end("Data not found");
            }
        });
    },
    get_cached_data: function () {
        return cache.get('trans_data');
    }
};
