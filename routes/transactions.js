/**
 * Created by Pradeep on 12/24/2015.
 */

var express = require('express');
var router = express.Router();

var database = require('./database');

router.get('/', function(req, res, next) {
    database.view_transactions(res);
});

module.exports = router;
