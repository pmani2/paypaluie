/**
 * Created by Pradeep on 12/28/2015.
 */
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database('database.sqlite3');

db.serialize(function() {
    db.run("CREATE TABLE if not exists transactions (id INTEGER PRIMARY KEY AUTOINCREMENT, from_email CHAR(255), " +
        "to_email CHAR(255), currency CHAR(10), amount REAL, message TEXT, trans_for INTEGER, datetime TEXT)");
});

module.exports = {
    save_transaction: function(email, sign, amount, message, trans_for){
        var date = new Date();
        db.run("INSERT into transactions(to_email, currency, amount, message, trans_for, datetime) VALUES (?,?,?,?,?,?)",
            [email, sign, amount, message, trans_for, date.toLocaleDateString()]);
    },

    view_transactions: function(res) {
        var trans = [];
        db.each("SELECT * FROM transactions", function(err, row) {
            trans.push({email: row.to_email, currency: row.currency, amount: row.amount, message: row.message,
                trans_for: row.trans_for, date: row.datetime});
        }, function() {
            res.render("transactions", {title: "Transactions", trans: trans});
        });
    }
};
