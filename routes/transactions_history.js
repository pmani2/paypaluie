/**
 * Created by Pradeep on 12/24/2015.
 */

var express = require('express');
var router = express.Router();
var cached_data = require('./caching');

router.get('/', function (req, res, next) {
    var data = cached_data.get_cached_data();
    if (data) {
        console.log("Cached Data");
        res.render('transactions_history', {title: "Transactions History", data: data});
    } else {
        console.log("No data in cache");
        cached_data.read_trans_data(res);
    }
});

module.exports = router;
