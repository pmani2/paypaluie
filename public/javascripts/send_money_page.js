/**
 * Created by Pradeep on 12/24/2015.
 */

var change_currency = function (elem) {
    var sign;
    if (elem.value == "USD") {
        sign = "&#36;";
    } else if (elem.value == "EUR") {
        sign = "&#8364;";
    } else if (elem.value == "JPY") {
        sign = "&#165;";
    }
    $("#currency").html(sign);
};

var set_decimal = function (elem) {
    elem.value = (elem.value.replace('.', '') / 100).toFixed(2);
};

var check_keycode = function (event) {
    if (event.keyCode != 8 && (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
        event.preventDefault();
    }
};

var send_money = function () {
    $("#load_screen").addClass("active");
    $("#sendmoney_form").submit();
};
